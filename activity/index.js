// Activity Solution Step by Step Queries

// 1. Insert Single Room

db.rooms.insert({
	name: "sigle",
	acommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false,
});

// 2. Insert Multiple Rooms

db.rooms.insertMany([
	{
		name: "double",
		acommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false,
	},
	{
		name: "queen",
		acommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false,
	},
]);

// 3. Find Method to search for a room with the name "double"

db.rooms.find({
	name: "double",
});

// 4. Update the queen room and set the available rooms to 0

db.rooms.updateOne(
	{
		name: "queen",
	},
	{
		$set: {
			rooms_available: 0,
		},
	}
);

// 5. Delete all rooms that have 0 availability

db.rooms.deleteOne({
	rooms_available: 0,
});
